//
//  ContentView.swift
//  AdsExample
//
//  Created by Chris Watts on 15/02/2023.
//

import SwiftUI
import NumberEight
import Audiences

class MainMenuViewModel: ObservableObject {
    @Published var audiences = "Initialising..."
    
    var timer: Timer? = nil
    
    init() {
        let token = NumberEight.start(withApiKey: "8T5DH83OX22WXZWR2P5H83ILEPZ6", consentOptions: .withConsentToAll()) { (success, error) in
            if let error = error {
                print("NumberEight failed to start with an error: \(error)")
            } else if success {
                print("NumberEight started successfully.")
            } else {
                print("NumberEight failed to start with an unknown error.")
            }
        }
        Audiences.startRecording(apiToken: token) { result in
            DispatchQueue.main.async {
                switch(result) {
                case .success():
                    self.audiences = "Thinking..."
                    self.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { _ in
                        self.updateAudiences()
                    }
                    break
                case let .failure(error):
                    self.audiences = error.localizedDescription
                    break
                }
            }
        }
    }
    
    @MainActor func updateAudiences() {
        let memberships = Audiences.currentMemberships
        if memberships.isEmpty {
            return
        }
        audiences = memberships.map { $0.name }.sorted().joined(separator: "\n")
    }
}

struct MainMenu: View {
    let controlWidth: CGFloat = 170
    let controlHeight: CGFloat = 30
    
    @ObservedObject var viewModel = MainMenuViewModel()
    
    let adsMenu = AdsMenu()

    var body: some View {
        NavigationView {
            ZStack(alignment: .bottom) {
                AccentShape()
                    .foregroundColor(Color.init("PrimaryColor"))
                    .ignoresSafeArea()
                VStack(spacing: 20) {
                    Image("Logo")
                        .resizable()
                        .scaledToFit()
                        .padding(EdgeInsets(top: 5, leading: 30, bottom: 5, trailing: 30))
                    VStack(spacing: 0) {
                        Text("Current Audiences").bold()
                        TextEditor(text: $viewModel.audiences)
                            .disabled(true)
                            .multilineTextAlignment(.center)
                            .frame(minHeight: 200)
                            .onAppear() {
                                UITextView.appearance().backgroundColor = .clear
                            }
                    }.padding(10)
                        .background(Color.init(UIColor.systemGray5))
                        .padding()
                    NavigationLink(destination: adsMenu) {
                        Text("Test Ads").frame(width: controlWidth, height: controlHeight)
                    }.buttonStyle(BaseButton())
                }.padding(.bottom, 50)
            }
        }
    }
}

struct MainMenu_Previews: PreviewProvider {
    static var previews: some View {
        MainMenu()
    }
}
