//
//  AdLoader.swift
//  AdsExample
//
//  Created by Chris Watts on 16/02/2023.
//

import Foundation
import PromiseKit
import CancellablePromiseKit

extension AdLoader {
    func closeAll() {
        closeBanner()
        cancelInterstitial()
    }
    
    internal func wrapPromise<T>(
        _ targetPromise: inout CancellablePromise<T>?,
        resolver targetResolver: inout Resolver<T>?,
        action: (CancellablePromise<T>, Resolver<T>) -> Void
    ) -> CancellablePromise<T> {
        if let promise = targetPromise, promise.isPending {
            return promise
        }

        let (promise, resolver) = Promise<T>.pending()
        let cancellablePromise = CancellablePromise(using: promise, cancel: { })
        targetPromise = cancellablePromise
        targetResolver = resolver
        action(cancellablePromise, resolver)

        return cancellablePromise
    }
}

enum AdLoaderError : Error {
    case adLoadFailed(String)
}
