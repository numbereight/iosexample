//
//  AccentShape.swift
//  AdsExample
//
//  Created by Chris Watts on 15/02/2023.
//

import Foundation
import SwiftUI

struct AccentShape: Shape {
    func path(in rect: CGRect) -> Path {
        var path = Path()

        path.move(to: CGPoint(x: rect.minX, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.maxY * 0.9))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY * 0.75))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))

        return path
    }
}

struct BaseButton: ButtonStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
        BaseButton(configuration: configuration)
    }
    
    struct BaseButton: View {
        let configuration: ButtonStyle.Configuration

        @Environment(\.isEnabled) private var isEnabled: Bool
        
        var body: some View {
            configuration.label
                .padding(7)
                .background(Color.init(
                    configuration.isPressed || !isEnabled
                        ? UIColor.systemGray4
                        : UIColor.systemGray5
                ))
                .foregroundColor(isEnabled ? Color.black : Color.gray)
        }
    }
}
