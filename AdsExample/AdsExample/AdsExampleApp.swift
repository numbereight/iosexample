//
//  AdsExampleApp.swift
//  AdsExample
//
//  Created by Chris Watts on 15/02/2023.
//

import SwiftUI

@main
struct AdsExampleApp: App {
    var body: some Scene {
        WindowGroup {
            MainMenu()
        }
    }
}
