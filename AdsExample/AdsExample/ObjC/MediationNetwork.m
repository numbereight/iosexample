//
//  MediationNetwork.m
//  AdsExample
//
//  Created by Chris Watts on 24/02/2023.
//

#import <Foundation/Foundation.h>
#import "AdsExample-Swift.h"
#import "MediationNetwork.h"

@implementation MediationNetwork {
    NSMutableSet* mediationUpdateHandlers;
}

@synthesize mediatedNetwork = _mediatedNetwork;

- (instancetype)init {
    if (self = [super init]) {
        self->mediationUpdateHandlers = [[NSMutableSet alloc] init];
    }
    return self;
}

- (NSArray<NSString*>*)mediationOptions {
    return @[@"Default"];
}

- (NSString*)mediatedNetwork {
    return _mediatedNetwork;
}

- (void)setMediatedNetwork:(NSString*)mediatedNetwork {
    if (![self.mediationOptions containsObject:mediatedNetwork]) {
        @throw([NSException exceptionWithName:@"Unknown mediation option" reason:@"The mediated network given is unrecognised" userInfo:@{
            @"Network": mediatedNetwork
        }]);
    }
    _mediatedNetwork = mediatedNetwork;

    for (MediatedNetworkUpdatedHandler handler in self->mediationUpdateHandlers) {
        handler(mediatedNetwork);
    }
}

- (void)addMediatedNetworkUpdatedHandler:(MediatedNetworkUpdatedHandler)handler {
    [self->mediationUpdateHandlers addObject:handler];
}

- (void)removeMediatedNetworkUpdatedHandler:(MediatedNetworkUpdatedHandler)handler {
    [self->mediationUpdateHandlers removeObject:handler];
}

@end
