//
//  GoogleAdManager.m
//  AdsExample
//
//  Created by Chris Watts on 24/02/2023.
//

#import <Foundation/Foundation.h>
#import "AdsExample-Swift.h"
#import "GoogleAdManager.h"
#import "Utils.h"

@import Audiences;
@import GoogleMobileAds;
@import PromiseKit;

@implementation GoogleAdManagerObjC {
    GAMBannerView* bannerView;
    UIViewController* viewController;
    
    AnyPromise* bannerPromise;
    PMKResolver bannerResolver;
    AnyPromise* interstitialPromise;
    PMKResolver interstitialResolver;
}

- (instancetype)initWithBannerView:(GAMBannerView *)bannerView {
    if (self = [super init]) {
        self->bannerView = bannerView;
        [[GADMobileAds sharedInstance] startWithCompletionHandler:nil];
        [self setupAdUnits];
    }
    return self;
}

- (NSString*)name {
    return @"Google Ad Manager";
}

- (void)setupAdUnits {
    bannerView.delegate = self;
}

- (GAMRequest*)generateRequest {
    /// Begin CodeSnippet: CollectMemberships
    // Collect audiences for all liveness types
    NSSet<NEXMembership*>* memberships = [NEXAudiences currentMemberships];
    NSSet<NEXMembership*>* habitual = [memberships filteredSetUsingPredicate:
                                       [NSPredicate predicateWithBlock:^BOOL(id m, id bindings) {
        return [m liveness] == kNEXLivenessStateHabitual;
    }]];
    NSSet<NEXMembership*>* live = [memberships filteredSetUsingPredicate:
                                   [NSPredicate predicateWithBlock:^BOOL(id m, id bindings) {
        return [m liveness] == kNEXLivenessStateLive;
    }]];
    NSSet<NEXMembership*>* today = [memberships filteredSetUsingPredicate:
                                    [NSPredicate predicateWithBlock:^BOOL(id m, id bindings) {
        return [m liveness] == kNEXLivenessStateHappenedToday;
    }]];
    NSSet<NEXMembership*>* thisWeek = [memberships filteredSetUsingPredicate:
                                       [NSPredicate predicateWithBlock:^BOOL(id m, id bindings) {
        return [m liveness] == kNEXLivenessStateHappenedThisWeek;
    }]];
    NSSet<NEXMembership*>* thisMonth = [memberships filteredSetUsingPredicate:
                                        [NSPredicate predicateWithBlock:^BOOL(id m, id bindings) {
        return [m liveness] == kNEXLivenessStateHappenedThisMonth;
    }]];
    /// End CodeSnippet: CollectMemberships

    /// Begin CodeSnippet: MakeGAMRequest
    // Create an ad request filled with NumberEight Audiences Taxonomy IDs
    // Audiences build up over time: live audiences are available after a few seconds, whereas
    // habitual ones can take several days.
    // While testing, it is recommended to continually make new ad requests to see as many
    // audiences as possible.
    GAMRequest* request = [[GAMRequest alloc] init];
    [request setCustomTargeting: @{
        @"ne_habitual" : [[[habitual valueForKey:@"ID"] allObjects] componentsJoinedByString:@","],
        @"ne_live" : [[[live valueForKey:@"ID"] allObjects] componentsJoinedByString:@","],
        @"ne_today" : [[[today valueForKey:@"ID"] allObjects] componentsJoinedByString:@","],
        @"ne_this_week" : [[[thisWeek valueForKey:@"ID"] allObjects] componentsJoinedByString:@","],
        @"ne_this_month" : [[[thisMonth valueForKey:@"ID"] allObjects] componentsJoinedByString:@","]
    }];
    /// End CodeSnippet: MakeGAMRequest

    return request;
}

#pragma mark - AdLoader

- (AnyPromise * _Nonnull)loadBanner {
    return wrapPromise(&bannerPromise, &bannerResolver, ^void (AnyPromise* promise, PMKResolver resolver) {
        GAMRequest* request = [self generateRequest];
        GAMBannerView* bannerView = self->bannerView;
        /// Begin CodeSnippet: LoadGAMBannerAd
        [bannerView loadRequest:request];
        /// End CodeSnippet: LoadGAMBannerAd
    });
}

- (void)closeBanner {
    if (bannerResolver != nil) {
        bannerResolver(cancellationError());
    }
    [bannerView setHidden:true];
}

- (AnyPromise * _Nonnull)loadInterstitial {
    return wrapPromise(&interstitialPromise, &interstitialResolver, ^void (AnyPromise* promise, PMKResolver resolver) {
        GAMRequest* request = [self generateRequest];
        [GAMInterstitialAd loadWithAdManagerAdUnitID:[GoogleAdManager interstitialAdUnitId]
                                             request:request
                                   completionHandler:^(GAMInterstitialAd * _Nullable ad, NSError * _Nullable error) {
            if (error != nil) {
                resolver(error);
                return;
            }

            UIViewController* viewController = [self->bannerView rootViewController];
            if (viewController == nil) {
                resolver(adLoadError(@"No view controller available to show interstitial ad"));
                return;
            }
            
            if (ad == nil) {
                resolver(adLoadError(@"Ad is null"));
                return;
            }
            
            if ([promise pending]) {
                [ad presentFromRootViewController:viewController];
                resolver(nil);
            }
        }];
    });
}

- (void)cancelInterstitial {
    if (interstitialResolver != nil) {
        interstitialResolver(cancellationError());
    }
}

#pragma mark - GADBannerViewDelegate

- (void)bannerViewDidReceiveAd:(GADBannerView * _Nonnull)bannerView {
    if (bannerPromise != nil && bannerResolver != nil && [bannerPromise pending]) {
        [bannerView setHidden:false];
        bannerResolver(nil);
    }
}

- (void)bannerView:(GADBannerView * _Nonnull)bannerView didFailToReceiveAdWithError:(NSError * _Nonnull)error {
    if (bannerResolver != nil) {
        bannerResolver(error);
    }
}

@end
