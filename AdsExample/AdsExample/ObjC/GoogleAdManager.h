//
//  GoogleAdManager.h
//  AdsExample
//
//  Created by Chris Watts on 06/03/2023.
//
#ifndef GoogleAdManager_h
#define GoogleAdManager_h

#include "AdLoader.h"
#include "MediationNetwork.h"

@import GoogleMobileAds;

@class MediationNetwork;

@interface GoogleAdManagerObjC : MediationNetwork <AdLoader>

- (nonnull instancetype)initWithBannerView:(GAMBannerView* _Nonnull)bannerView;

@end

@interface GoogleAdManagerObjC (GADBannerViewDelegateExtension) <GADBannerViewDelegate>

- (void)bannerViewDidReceiveAd:(GADBannerView * _Nonnull)bannerView;
- (void)bannerView:(GADBannerView * _Nonnull)bannerView didFailToReceiveAdWithError:(NSError * _Nonnull)error;

@end

#endif /* GoogleAdManager_h */
