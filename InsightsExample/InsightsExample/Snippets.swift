//
//  Snippets.swift
//  InsightsExample
//
//  Created by Chris Watts on 30/10/2019.
//  Copyright © 2019 NumberEight. All rights reserved.
//

/// Begin CodeSnippet: ImportInsights
import Insights
/// End CodeSnippet: ImportInsights

class InitializeSDK : UIViewController {
/// Begin CodeSnippet: InitializeInsights
func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    let token = NumberEight.start(withApiKey: "REPLACE_WITH_DEVELOPER_KEY",
                                  launchOptions: launchOptions,
                                  consentOptions: ConsentOptions.withConsentToAll()) { (success, error) in
        if let error = error {
            print("NumberEight failed to start with an error: \(error)")
        } else if success {
            print("NumberEight started successfully.")
        } else {
            print("NumberEight failed to start with an unknown error.")
        }
    }

    Insights.startRecording(apiToken: token) { (result) in
        if case .failure(let error) = result {
            print("NumberEight Insights failed to start with an error: \(error)")
        } else {
            print("NumberEight Insights started successfully.");
        }
    }

    return true
}
/// End CodeSnippet: InitializeInsights
}

/// Begin CodeSnippet: InsightsMarkerEvents
class MyInsightsAppSwift : UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        Insights.addMarker("screen_viewed")
    }

    func purchaseMade(value: Int) {
        Insights.addMarker("in_app_purchase", parameters: [
            "value": value
        ])
    }

    func songPlayed(title: String, artist: String, genre: String) {
        Insights.addMarker("song_played", parameters: [
            "title": title,
            "artist": artist,
            "genre": genre
        ])
    }
}
/// End CodeSnippet: InsightsMarkerEvents

func useDeviceId(token: APIToken) {
    /// Begin CodeSnippet: InsightsSpecifyDeviceID
    let config = RecordingConfig.default
    let deviceId = "insert_custom_device_or_user_id_here_if_required"
    config.deviceId = deviceId

    Insights.startRecording(apiToken: token, config: config) { (result) in
        if case .failure(let error) = result {
            print("NumberEight Insights failed to start with an error: \(error)")
        } else {
            print("NumberEight Insights started successfully.");
        }
    }
    /// End CodeSnippet: InsightsSpecifyDeviceID
}


func addContexts(token: APIToken) {
    /// Begin CodeSnippet: InsightsSpecifyConfig
    let config = RecordingConfig.default

    // Add the Device Movement context to the list of recorded topics
    config.topics.insert(kNETopicDeviceMovement)

    // Put a smoothing filter on the Device Movement context
    let parameters = Parameters.sensitivitySmoother.and(Parameters.changesOnly)
    config.filters[kNETopicDeviceMovement] = parameters.filter

    Insights.startRecording(apiToken: token, config: config) { (result) in
        if case .failure(let error) = result {
            print("NumberEight Insights failed to start with an error: \(error)")
        } else {
            print("NumberEight Insights started successfully.");
        }
    }
    /// End CodeSnippet: InsightsSpecifyConfig
}
